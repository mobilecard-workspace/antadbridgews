package com.addcel.antad.bridge.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class AntadResponse{

	private int idError;
	
	private String mensajeError;
	
	private double monto;
	
	private String descripcion;
	
	private String numAutorizacion;
		
	private String mensajeAntad;
	
	private String referencia;
	
	private String concepto;
	
	private long idTransaccion;
	
	private double comision;
	
	private String eMail;
	
	private String respCode;
	
	private String numAuth;
	
	private String mensajeTicket;
	
	private String mensajeCajero;
	
	private String folioTransaccion;
	
	private String folioComercio;
	
	private String datosAdicionales;
	
	private double montoUsd;
	
	private double comisionUsd;
	
	private double montoMxn;
	
	private double comisionMxn;
	
	private double totalUsd;
	
	private double totalMxn;
	
	private double tipoCambio;
	

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNumAutorizacion() {
		return numAutorizacion;
	}

	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getMensajeAntad() {
		return mensajeAntad;
	}

	public void setMensajeAntad(String mensajeAntad) {
		this.mensajeAntad = mensajeAntad;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public long getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getNumAuth() {
		return numAuth;
	}

	public void setNumAuth(String numAuth) {
		this.numAuth = numAuth;
	}

	public String getMensajeTicket() {
		return mensajeTicket;
	}

	public void setMensajeTicket(String mensajeTicket) {
		this.mensajeTicket = mensajeTicket;
	}

	public String getMensajeCajero() {
		return mensajeCajero;
	}

	public void setMensajeCajero(String mensajeCajero) {
		this.mensajeCajero = mensajeCajero;
	}

	public String getFolioTransaccion() {
		return folioTransaccion;
	}

	public void setFolioTransaccion(String folioTransaccion) {
		this.folioTransaccion = folioTransaccion;
	}

	public String getFolioComercio() {
		return folioComercio;
	}

	public void setFolioComercio(String folioComercio) {
		this.folioComercio = folioComercio;
	}

	public String getDatosAdicionales() {
		return datosAdicionales;
	}

	public void setDatosAdicionales(String datosAdicionales) {
		this.datosAdicionales = datosAdicionales;
	}

	public double getMontoUsd() {
		return montoUsd;
	}

	public void setMontoUsd(double montoUsd) {
		this.montoUsd = montoUsd;
	}

	public double getComisionUsd() {
		return comisionUsd;
	}

	public void setComisionUsd(double comisionUsd) {
		this.comisionUsd = comisionUsd;
	}

	public double getMontoMxn() {
		return montoMxn;
	}

	public void setMontoMxn(double montoMxn) {
		this.montoMxn = montoMxn;
	}

	public double getComisionMxn() {
		return comisionMxn;
	}

	public void setComisionMxn(double comisionMxn) {
		this.comisionMxn = comisionMxn;
	}

	public double getTotalUsd() {
		return totalUsd;
	}

	public void setTotalUsd(double totalUsd) {
		this.totalUsd = totalUsd;
	}

	public double getTotalMxn() {
		return totalMxn;
	}

	public void setTotalMxn(double totalMxn) {
		this.totalMxn = totalMxn;
	}

	public double getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	
}
