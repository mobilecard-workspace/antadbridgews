package com.addcel.antad.bridge.vo;

public class Contact {

	private String Name;
	
	private String Telephone;

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getTelephone() {
		return Telephone;
	}

	public void setTelephone(String telephone) {
		Telephone = telephone;
	}
	
}
