package com.addcel.antad.bridge.vo;

import java.util.ArrayList;

public class DataSet {

	private ArrayList<Series> Series;
	
	private String SiblingGroup;

	public String getSiblingGroup() {
		return SiblingGroup;
	}

	public void setSiblingGroup(String siblingGroup) {
		SiblingGroup = siblingGroup;
	}

	public ArrayList<Series> getSeries() {
		return Series;
	}

	public void setSeries(ArrayList<Series> series) {
		Series = series;
	}
	
}
