package com.addcel.antad.bridge.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.antad.bridge.vo.AntadCredentials;
import com.addcel.antad.bridge.vo.AntadDetalle;
import com.addcel.antad.bridge.vo.AntadInfo;
import com.addcel.antad.bridge.vo.Bitacora;
import com.addcel.antad.bridge.vo.Usuario;

public class JdbcDao extends SqlMapClientDaoSupport{

	private static final Logger LOGGER = LoggerFactory.getLogger(JdbcDao.class);
	
	private static final String GET_USUARIO_INFO = "getUsuarioInfo";
	
	private static final String GET_COMISIONES = "getComisiones";
	
	private static final String ANTAD_CREDENTIALS = "antadCredenciales";
	
	private static final String INSERTA_TRANSACCION = "insertaTransaccion";
	
	private static final String ACTUALIZA_TRANSACCION = "actualizaTransaccion";
	
	private static final String INSERTA_TRANSACCION_DETALLE = "insertaAntadDetalle";
	
	private static final String ACTUALIZA_TRANSACCION_DETALLE = "actualizaAntadDetalle";

	private static final String CONSULTA_TRANSACCION_DETALLE = "consultaAntadDetalle";
	
	private static final String GET_DIVISAS = "getDivisas";
	
	private static final String UPDATE_DIVISAS = "updateDivisas";
	
	private static final String GET_COMISIONES_EMISOR = "getComisionxEmisor";
	
	private static final String GET_FOLIO_TRANSACCION = "getFolioTransaccion";
	
	public Usuario getUsuarioInfo(long idusuario){
		Usuario usuario = null;
		try {
			LOGGER.info("[JDBC] - BUSCANDO USUARIO: "+idusuario);
			usuario = (Usuario) getSqlMapClientTemplate().queryForObject(GET_USUARIO_INFO, idusuario);
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL CONSULTAR LA INFORMACION DEL USUARIO: "
					+idusuario+" CAUSA: "+e.getMessage());
		}
		return usuario;
	}
	
	public double obtenComision(int idProducto){
		Double comision = null;
		try {
			LOGGER.info("[JDBC] - CONSULTANDO COMISION - PRODUCTO: "+idProducto);
			comision = (Double) getSqlMapClientTemplate().queryForObject(GET_COMISIONES, idProducto);
			LOGGER.info("[JDBC] - COMISION ENCONTRADA PARA EL PRODUCTO: "+idProducto+" COMISION: "+comision);
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL CONSULTAR LA COMISION DEL PRODUCTO: "
					+idProducto+" EXCEPTION: "+e.getCause());
		}
		return comision.doubleValue();
	}
	
	public long insertaTransaccion(Bitacora bitacora){
		try {
			LOGGER.info("[JDBC] - INSERTANDO TRANSACCION");
			getSqlMapClientTemplate().insert(INSERTA_TRANSACCION, bitacora);
			LOGGER.info("[JDBC] - TRANSACCION ANTAD INSERTADA CON EXITO - ID TRANSACCION: "+bitacora.getIdTransaccion());
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL INSERTAR LA TRANSACCION DEL USUARIO: "
					+bitacora.getIdUsuario()+" EXCEPTION: "+e.getCause());
		}
		return bitacora.getIdTransaccion();
	}
	
	public void actualizaTransaccion(Bitacora bitacora){
		try {
			LOGGER.info("[JDBC] - ACTUALIZANDO TRANSACCION - ID TRANSACCION: "+bitacora.getIdTransaccion());
			getSqlMapClientTemplate().update(ACTUALIZA_TRANSACCION, bitacora);
			LOGGER.info("[JDBC] - TRANSACCION ANTAD ACTUALIZADA EXITOSAMENTE - ID TRANSACCION: "+bitacora.getIdTransaccion());
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL ACTUALIZAR LA TRANSACCION DEL USUARIO: "
					+bitacora.getIdTransaccion()+" EXCEPTION: "+e.getCause());
		}
	}
	
	public void insertaAntadDetalle(AntadDetalle antadDetalle){
		try {
			LOGGER.info("[JDBC] - INSERTANDO TRANSACCION ANTAD DETALLE: "+antadDetalle.getIdTransaccion());
			getSqlMapClientTemplate().insert(INSERTA_TRANSACCION_DETALLE, antadDetalle);
			LOGGER.info("[JDBC] - TRANSACCION ANTAD INSERTADA CON EXITO - ID TRANSACCION: "+antadDetalle.getIdTransaccion());
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL INSERTAR LA TRANSACCION DEL USUARIO: "
					+antadDetalle.getIdTransaccion()+" EXCEPTION: "+e.getCause());
		}
		return ;
	}
	
	public void actualizaAntadDetalle(AntadDetalle antadDetalle){
		try {
			LOGGER.info("[JDBC] - ACTUALIZANDO TRANSACCION ANTAD - ID TRANSACCION: "+antadDetalle.getIdTransaccion());
			getSqlMapClientTemplate().update(ACTUALIZA_TRANSACCION_DETALLE, antadDetalle);
			LOGGER.info("[JDBC] - TRANSACCION ANTAD ACTUALIZADA EXITOSAMENTE - ID TRANSACCION: "+antadDetalle.getIdTransaccion());
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL ACTUALIZAR LA TRANSACCION DEL USUARIO: "
					+antadDetalle.getIdTransaccion()+" EXCEPTION: "+e.getCause());
		}
	}
	
	public AntadCredentials antadCredentials(){
		AntadCredentials credenciales = null;
		try {
			LOGGER.info("[JDBC] - OBTENIENDO CREDENCIALES DE ANTAD...");
			credenciales = (AntadCredentials) getSqlMapClientTemplate().queryForObject(ANTAD_CREDENTIALS);
			LOGGER.info("[JDBC] - CREDENCIALES DE ANTAD - OK");
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL OBTENER LAS CREDENCIALES DE ANTAD - EXCEPTION: "+e.getCause());
		}
		return credenciales;
	}

	public AntadInfo getDetalleTransaccion(AntadInfo antadInfo) {
		AntadInfo info = null;
		try {
			LOGGER.info("[JDBC] -CONSULTANDO TRANSACCION ANTAD - ID TRANSACCION: "+antadInfo.getIdTransaccion());
			info = (AntadInfo) getSqlMapClientTemplate().queryForObject(CONSULTA_TRANSACCION_DETALLE, antadInfo);
			LOGGER.info("[JDBC] - TRANSACCION ANTAD ACTUALIZADA EXITOSAMENTE - ID TRANSACCION: "+antadInfo.getIdTransaccion());
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL ACTUALIZAR LA TRANSACCION DEL USUARIO: "
					+antadInfo.getIdTransaccion()+" EXCEPTION: "+e.getCause());
			e.printStackTrace();
		}
		return info;
	}
	
	public double obtenDivisa(String id){
		Double divisa = null;
		try {
			LOGGER.info("[JDBC] - CONSULTANDO DIVISA - PRODUCTO: "+id);
			divisa = (Double) getSqlMapClientTemplate().queryForObject(GET_DIVISAS, id);
			LOGGER.info("[JDBC] - DIVISA ENCONTRADA PARA EL PRODUCTO: "+id+" COMISION: "+divisa);
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL CONSULTAR LA DIVISA DEL PRODUCTO: "
					+id+" EXCEPTION: "+e.getCause());
		}
		return divisa.doubleValue();
	}

	public void updateDivisa(double dolar, String id) {
		try {
			LOGGER.info("[JDBC] - ACTUALIZANDO DIVISA - PRODUCTO: "+id);
			getSqlMapClientTemplate().queryForObject(UPDATE_DIVISAS, id);
			LOGGER.info("[JDBC] - DIVISA ACTUALIZANDO PARA EL PRODUCTO: "+id+" VALOR: "+dolar);
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL ACTUALIZANDO LA DIVISA DEL PRODUCTO: "
					+id+" EXCEPTION: "+e.getCause());
		}
	}
	
	public double getComisionXEmisor(String emisor){
		Double divisa = null;
		try {
			LOGGER.info("[JDBC] - CONSULTANDO COMISION - PRODUCTO: "+emisor);
			divisa = (Double) getSqlMapClientTemplate().queryForObject(GET_COMISIONES_EMISOR, emisor);
			LOGGER.info("[JDBC] - COMISION ENCONTRADA PARA EL PRODUCTO: "+emisor+" COMISION: "+divisa);
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL CONSULTAR LA COMISION DEL PRODUCTO: "
					+emisor+" EXCEPTION: "+e.getCause());
			divisa = new Double(0);
		}
		return divisa;
	}
	
	public String getFolioTransaccion(String referencia){
		String folioTransaccion = null;
		try {
			LOGGER.info("[JDBC] - CONSULTANDO FOLIO TRANSACCION - REFERENCIA: "+referencia);
			folioTransaccion = (String) getSqlMapClientTemplate().queryForObject(GET_FOLIO_TRANSACCION, referencia);
			LOGGER.info("[JDBC] - FOLIO TRANSACCION ENCONTRADA : "+folioTransaccion);
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL CONSULTAR FOLIO TRANSACCION REFERENCIA: "
					+referencia+" EXCEPTION: "+e.getCause());
		}
		return folioTransaccion;
	}
}
