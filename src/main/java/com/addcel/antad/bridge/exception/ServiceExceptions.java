/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.antad.bridge.exception;

import com.addcel.antad.bridge.utils.ServiceErrorEnumeration;

/**
 *
 * @author JOCAMPO
 */
public class ServiceExceptions extends Exception {

    private final String code;
    private final String description;

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public ServiceExceptions(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public ServiceExceptions(ServiceErrorEnumeration error) {
        this.code = error.getCodigo();
        this.description = error.getError();
    }

}
