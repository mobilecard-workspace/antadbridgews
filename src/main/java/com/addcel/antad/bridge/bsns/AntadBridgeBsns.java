package com.addcel.antad.bridge.bsns;

import static com.addcel.antad.bridge.utils.Constants.FORMATO_FECHA_ENCRIPT;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.xml.transform.StringSource;
import org.tempuri.WebServiceSoapProxy;

import com.addcel.antad.bridge.antad.Autorizacion;
import com.addcel.antad.bridge.antad.AutorizacionResponse;
import com.addcel.antad.bridge.antad.Consulta;
import com.addcel.antad.bridge.antad.ConsultaResponse;
import com.addcel.antad.bridge.antad.ObjectFactory;
import com.addcel.antad.bridge.antad.Registro;
import com.addcel.antad.bridge.antad.RegistroResponse;
import com.addcel.antad.bridge.dao.JdbcDao;
import com.addcel.antad.bridge.ibatis.AbstractService;
import com.addcel.antad.bridge.utils.JsonMapper;
import com.addcel.antad.bridge.vo.AntadCredentials;
import com.addcel.antad.bridge.vo.AntadDetalle;
import com.addcel.antad.bridge.vo.AntadInfo;
import com.addcel.antad.bridge.vo.AntadResponse;
import com.addcel.antad.bridge.vo.Bitacora;
import com.addcel.antad.bridge.vo.Usuario;
import com.addcel.utils.AddcelCrypto;

public class AntadBridgeBsns extends AbstractService{

	private static final Logger LOGGER = LoggerFactory.getLogger(AntadBridgeBsns.class);
	
	private static final SimpleDateFormat SD = new SimpleDateFormat(FORMATO_FECHA_ENCRIPT);
	
	private static final String JDBC_DAO = "JdbcDao"; 
	
	private static final String ANTAD_WS_BEAN = "webServiceTemplateAntad"; 
	
    private WebServiceTemplate webServiceTemplate;
    
//    private WebServiceTemplate webServiceBanxico;
	
	private JsonMapper jsonUtils;
	
	private JdbcDao dao;
	
	public AntadBridgeBsns() {
		try {
			webServiceTemplate = (WebServiceTemplate) getBean(ANTAD_WS_BEAN);
			//QA
			//webServiceTemplate.setDefaultUri("https://192.168.169.152:9084/wsantad.asmx");
			//PROD 192.168.169.153
			webServiceTemplate.setDefaultUri("https://192.168.169.153:9084/wsantad.asmx");
//			webServiceBanxico = (WebServiceTemplate) getBean("webServiceBanxico");
			dao = (JdbcDao) getBean(JDBC_DAO);
			jsonUtils = new JsonMapper();
		} catch (Exception e) {
			LOGGER.info("NO SE PUDO INSTANCIAR LA CLASE ");
			e.printStackTrace();
		}
	}
	
	static{
		javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
		new javax.net.ssl.HostnameVerifier(){

		    public boolean verify(String dns,
		            javax.net.ssl.SSLSession sslSession) {
		        return true;
		    }
		});
	}
	
	public String registro(String json) {
		Registro registro = new Registro();
		RegistroResponse response = null;
		org.tempuri.RegistrosResponse axisResp = null;
		try {
			registro.setComercio("8315");
			registro.setMacAddress("0200c0a84b36");
			registro.setSucursal("1");
			LOGGER.info("DATOS ENVIADO A ANTAD  - {}",jsonUtils.objectToJson(registro));
//			WebServiceSoapProxy proxy = new WebServiceSoapProxy();
//			axisResp = proxy.registro("8315", "1", "0200c0a84b36");
//			LOGGER.info("RESPUESTA DE AXIS - {}", jsonUtils.objectToJson(axisResp));
			ObjectFactory factory = new ObjectFactory();
			registro = factory.createRegistro();
			registro.setComercio("8315");
			registro.setMacAddress("0200c0a84b36");
			registro.setSucursal("1");
			response = (RegistroResponse) webServiceTemplate.marshalSendAndReceive(registro, new WebServiceMessageCallback() {
		        public void doWithMessage(WebServiceMessage message) {
		            try {
		                SoapMessage soapMessage = (SoapMessage)message;
		                SoapHeader header = soapMessage.getSoapHeader();
		                StringSource headerSource = new StringSource("<tem:UserCredential xmlns:tem=\"http://tempuri.org/\">" +
		                        "<tem:UserName>8315usu0001</tem:UserName> "+
		                        "<tem:Password> </tem:Password> "+
		                        "<tem:Token> </tem:Token> "+
		                        "<tem:MacAddress>0200c0a84b36</tem:MacAddress>  "+
		                        "<tem:Comercio>8315</tem:Comercio>  "+
		                        "</tem:UserCredential>");
		                Transformer transformer = TransformerFactory.newInstance().newTransformer();
		                transformer.transform(headerSource, header.getResult());
		            } catch (Exception e) {
		                e.printStackTrace();
		            }
		        }
		    });
			LOGGER.info("REPUESTA RECIBIDA DE ANTAD  - {}",jsonUtils.objectToJson(response.getRegistroResult()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public String autorizacion(String json) {
		AntadResponse resp = null;
		AntadInfo antadInfo;
		AntadInfo info = null;
		try {
			json = procesaEntrada(json);
			LOGGER.info("*** COMENZANDO AUTORIZACION ANTAD ***" + json);
			info = (AntadInfo) jsonUtils.jsonToObject(json, AntadInfo.class);
			antadInfo = dao.getDetalleTransaccion(info);
			antadInfo.setIdPais(info.getIdPais());
			resp = (AntadResponse) aprovisionaAntad(antadInfo);
			if(!"00".equals(resp.getRespCode())){
				if("TO".equals(resp.getRespCode())){
					resp = (AntadResponse) aprovisionaAntad(antadInfo);
					if(!"00".equals(resp.getRespCode()) || !"PA".equals(resp.getRespCode())){
						resp.setIdError(-1);
						resp.setConcepto("Error con el proveedor - "+antadInfo.getConcepto());
					}
				}
				resp.setIdError(-1);
				resp.setConcepto("Error con el proveedor - "+antadInfo.getConcepto());
			} else {
				resp.setConcepto(antadInfo.getConcepto());
			}
			resp.setReferencia(antadInfo.getReferencia());
			resp.setComision(antadInfo.getComision());
			resp.seteMail(antadInfo.geteMail());
			resp.setMonto(antadInfo.getMonto());
			actualizaAntadDetalle(resp, antadInfo);
			resp.setComision(10);
			resp.setReferencia("12345678");
			resp.setConcepto("Pago de Servicios");
			resp.setNumAuth("1234567");
			resp.setMonto(antadInfo.getMonto());
			resp.setComision(antadInfo.getComision());
			json = jsonUtils.objectToJson(resp);
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL REALIZAR LA AUTORIZACION CON ANTAD - CAUSA: "+e.getCause());
			e.printStackTrace();
		} finally {
			LOGGER.info("RESPUESTA DE AUTORIZACION ANTAD: "+json);
			json = procesaRespuesta(json);
		}
		return json;
	}
	
	
	//29/11/2019 - 11:33:11 INFO : [com.addcel.antad.bridge.bsns.AntadBridgeBsns.109] - REPUESTA RECIBIDA DE ANTAD  - 
	// {"registroResult":{"respCode":"00","comercio":"8315","sucursal":"1","claveAcceso":"itf[o{z57mQd0/{^",
	// "token":"3E757E10455DA6C231F9E520987AE4A4","mensajeCajero":"PROCESO COMPLETO"}}
	private AntadResponse aprovisionaAntad(AntadInfo info){
		AutorizacionResponse response = null; 
		Autorizacion requestWS;
		ObjectFactory factory = new ObjectFactory();
		AntadCredentials credentials;
		AntadResponse resp = new AntadResponse();
		double comision = 0;
		String folioTran = null;
		try {
			LOGGER.info("ANTAD INFO - {}", jsonUtils.objectToJson(info));
			credentials = (AntadCredentials) dao.antadCredentials();
			requestWS = factory.createAutorizacion();
			requestWS.setComercio(credentials.getComercio());
			requestWS.setSucursal(credentials.getSucursal());
			requestWS.setCaja(credentials.getCaja());
			requestWS.setTicket(String.valueOf(info.getIdTransaccion()));
			requestWS.setFolioComercio(info.getIdTransaccion());
			requestWS.setOperacion(credentials.getOperacionAutorizacion());
			requestWS.setReferencia(info.getReferencia());
			requestWS.setCajero("");
			requestWS.setHorario("1");
			requestWS.setFAC("");
			requestWS.setReferencia3("");
			LOGGER.info("VALIDANDO EL ID PAIS: "+info.getIdPais());
			if("2".equals(info.getIdPais())){
				double valorDolar = dao.obtenDivisa("1");
				double total = info.getMonto() * valorDolar;
				String montoS = String.format("%.2f", total);
				LOGGER.info("PAGO EN DOLARES CALCULANDO PESOS - PAGO EN DOLARES: "+info.getMonto()+" - TOTAL: "+total+" - MONTO STRING: "+montoS);
				requestWS.setMonto(Double.valueOf(montoS));
				consultaDivisaBanxico(requestWS, info.getMonto());
			} else {
				requestWS.setMonto(info.getMonto());
			}
			
			requestWS.setEmisor(info.getEmisor());
			requestWS.setModoIngreso(credentials.getModoIngreso());
			comision = dao.getComisionXEmisor(info.getEmisor());
			requestWS.setComision(formatearDecimales(comision, 2));
			if(info.getConcepto().contains("RECARGA")){
				requestWS.setOperacion("000033");
			} else {
				requestWS.setOperacion(credentials.getOperacionAutorizacion());
			}
			//'200', '244', '213'
			if(info.getEmisor().equals("200") || info.getEmisor().equals("244")
					|| info.getEmisor().equals("213")) {
				folioTran = dao.getFolioTransaccion(info.getReferencia());
				requestWS.setReferencia2(folioTran);
			} else if(info.getEmisor().equals("261")){
				requestWS.setReferencia2("");
			} else {
				requestWS.setReferencia2(info.getFolioTransaccion());
			}
			
			response = (AutorizacionResponse) webServiceTemplate.marshalSendAndReceive(requestWS, new WebServiceMessageCallback() {

		        public void doWithMessage(WebServiceMessage message) {
		            try {
		                SoapMessage soapMessage = (SoapMessage)message;
		                SoapHeader header = soapMessage.getSoapHeader();
		                StringSource headerSource = new StringSource("<tem:UserCredential xmlns:tem=\"http://tempuri.org/\">" +
		                        "<tem:UserName>8315usu0001</tem:UserName>"+
		                        "<tem:Password>TLar.ohazuCPQ:\\J</tem:Password>"+
		                        "<tem:Token>0D41C6316ACB64F6A67F8FDB1CFC1102</tem:Token>"+
		                        "<tem:MacAddress>0200c0a84b36</tem:MacAddress>"+
		                        "<tem:Comercio>8315</tem:Comercio>"+
		                        "</tem:UserCredential>");
		                Transformer transformer = TransformerFactory.newInstance().newTransformer();
		                transformer.transform(headerSource, header.getResult());
		            } catch (Exception e) {
		                e.printStackTrace();
		            }
		        }
		    });
			
			//BeanUtils.copyProperties(response.getAutorizacionResult(), resp);
			resp.setRespCode(response.getAutorizacionResult().getRespCode());
			resp.setFolioTransaccion(response.getAutorizacionResult().getFolioTransaccion());
			resp.setNumAuth(response.getAutorizacionResult().getNumAuth());
		} catch (Exception ex){ 
			LOGGER.error("OCURRIO UN ERROR AL COMUNICARSE CON ANTAD: "+ex.getCause());
			ex.printStackTrace();
			if(ex.getClass().isInstance(java.net.SocketException.class)){
				resp.setRespCode("TO");
			} else {
				resp.setRespCode("ER");
			}
			resp.setMensajeAntad("ERROR CON ANTAD - DEVOLUCION");
			resp.setMensajeTicket("ERROR ANTAD");
			resp.setMensajeCajero("ERROR ANTAD");
			resp.setIdTransaccion(0);
		}
		return resp;
	}
	
	private void consultaDivisaBanxico(Autorizacion requestWS, double monto) {
		String result = null;
//		SAXBuilder builder = new SAXBuilder();
//        try {
//        	com.addcel.antad.bridge.client.banxico.DgieWSPortProxy proxy = new com.addcel.antad.bridge.client.banxico.DgieWSPortProxy();
//        	result = proxy.tiposDeCambioBanxico();
//            try {
//            	Document document = (Document) builder.build(result);
//            	Element rootNode = ((org.jdom.Document) document).getRootElement();
//            	List<?> list = rootNode.getChildren("DataSet");
//            	for (int i = 0; i < list.size(); i++) {
// 				   Element node = (Element) list.get(i);
// 				   LOGGER.info("SiblingGroup: "+node.getChildText("SiblingGroup"));
// 				   LOGGER.info("Series: "+node.getChildText("Series"));
//            	}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//        } catch (Exception ex) {
//            
//        }
	}

	public String consulta(String json) {
		AntadResponse resp = null;
		ConsultaResponse response = null;
		AntadInfo antadInfo;
		Consulta requestWS = null;		
		ObjectFactory factory = new ObjectFactory();
		AntadCredentials credentials;
		String []datosAdicionales = null;
		String datos = null;
		double comision = 0;
		Usuario usuario = null;
		try {
			json = procesaEntrada(json);
			LOGGER.info("*** COMENZANDO CONSULTA DE SALDOS CON ANTAD ***" + json);
			jsonUtils = new JsonMapper();
			credentials = (AntadCredentials) dao.antadCredentials();
			antadInfo = (AntadInfo) jsonUtils.jsonToObject(json, AntadInfo.class);
			LOGGER.info("*** DATOS DE TRANSACCION *** {}", jsonUtils.objectToJson(antadInfo));
			requestWS = factory.createConsulta();
			requestWS.setComercio(credentials.getComercio());
			requestWS.setSucursal(credentials.getSucursal());
			requestWS.setCaja(credentials.getCaja());
			requestWS.setTicket(String.valueOf(antadInfo.getIdTransaccion()));
			requestWS.setFolioComercio(1l);
			requestWS.setOperacion(credentials.getOperacionConsulta());
			requestWS.setReferencia(antadInfo.getReferencia());
			requestWS.setReferencia2(" ");
			requestWS.setReferencia3(" ");
			requestWS.setMonto(new BigDecimal(0));
			requestWS.setEmisor(antadInfo.getEmisor());
			requestWS.setModoIngreso(credentials.getModoIngreso());
			requestWS.setComision(new BigDecimal(0));
			requestWS.setCajero("1");
			requestWS.setHorario("1");
			requestWS.setFAC(" ");
			requestWS.setReintento(0);
			response = (ConsultaResponse) webServiceTemplate.marshalSendAndReceive(requestWS, new WebServiceMessageCallback() {

		        public void doWithMessage(WebServiceMessage message) {
		            try {
		                SoapMessage soapMessage = (SoapMessage)message;
		                SoapHeader header = soapMessage.getSoapHeader();
		                StringSource headerSource = new StringSource("<tem:UserCredential xmlns:tem=\"http://tempuri.org/\">" +
		                        "<tem:UserName>8315usu0001</tem:UserName>"+
		                        "<tem:Password>TLar.ohazuCPQ:\\J</tem:Password>"+
		                        "<tem:Token>0D41C6316ACB64F6A67F8FDB1CFC1102</tem:Token>"+
		                        "<tem:MacAddress>0200c0a84b36</tem:MacAddress>"+
		                        "<tem:Comercio>8315</tem:Comercio>"+
		                        "</tem:UserCredential>");
		                Transformer transformer = TransformerFactory.newInstance().newTransformer();
		                transformer.transform(headerSource, header.getResult());
		            } catch (Exception e) {
		                e.printStackTrace();
		            }
		        }
		    });
			resp = new AntadResponse();
			if(response.getConsultaResult() != null) {
				if("00".equals(response.getConsultaResult().getRespCode())){
					resp.setRespCode(response.getConsultaResult().getRespCode());
					resp.setIdError(0);
					resp.setMensajeAntad(response.getConsultaResult().getMensajeTicket().trim());
					resp.setNumAutorizacion(response.getConsultaResult().getNumAuth());
					
					datos = response.getConsultaResult().getDatosAdicionales();
					datosAdicionales = datos.split("\\|");
					LOGGER.info("[ANTAD CONSULTA] - DATOS ADICIONALES: "+datos);
					LOGGER.info("[ANTAD CONSULTA] - LENGTH: "+datosAdicionales.length);
					
					double monto = 0;
					
					if(datosAdicionales.length > 2){
						if(!"1".equals(antadInfo.getIdPais())){
							double valorDolar = dao.obtenDivisa("1");
							resp.setTipoCambio(valorDolar);
////						resp.setMonto(Double.parseDouble(datosAdicionales[2]) / valorDolar);
//							double valorDolar = calculaDivisa(resp, datosAdicionales[2]);
//							double valorDolar = calculaDivisa(resp, datosAdicionales[2]);
							monto = Double.valueOf(datosAdicionales[2]);
							double montoUsd = monto / valorDolar;
							double comisionUsd = montoUsd * 0.05;
							double totalUsd = montoUsd + comisionUsd;
							resp.setMontoMxn(Double.parseDouble(datosAdicionales[2]));
							resp.setMontoUsd(formatearDecimales(montoUsd, 2));
							resp.setMonto(formatearDecimales(montoUsd, 2));
							
							resp.setComisionUsd(formatearDecimales(comisionUsd, 2));
							resp.setComision(formatearDecimales(comisionUsd, 2));
							resp.setComisionMxn(formatearDecimales(comisionUsd, 2));
							
							resp.setTotalUsd(formatearDecimales(totalUsd, 2));
							resp.setTotalMxn(formatearDecimales(totalUsd, 2));
						} else {
							monto = Double.valueOf(datosAdicionales[2]);
							resp.setMonto(Double.parseDouble(datosAdicionales[2]));
							resp.setMontoMxn(Double.parseDouble(datosAdicionales[2]));
							LOGGER.info("MONTO: "+resp.getMonto());
							LOGGER.info("MONTO MXN: "+resp.getMontoMxn());
							comision =  monto * 0.05;
							resp.setComision(formatearDecimales(comision, 2));
							resp.setComisionMxn(formatearDecimales(comision, 2));
							resp.setTotalMxn(formatearDecimales(monto + comision, 2));
						}					
					}
					if(resp.getMonto() == 0) {
						resp.setIdError(-1);
						resp.setMensajeError("Su saldo esta al dia. Saldo $0.00");
					}
					
					
					resp.setFolioTransaccion(response.getConsultaResult().getFolioTransaccion());
					antadInfo.setFolioTransaccion(response.getConsultaResult().getFolioTransaccion());
					usuario = getUsuario(antadInfo.getIdUsuario());
					antadInfo.setIdTransaccion(insertaTransaccion(antadInfo, usuario));
					insertaAntadDetalle(antadInfo);
				} else {
					resp.setRespCode(response.getConsultaResult().getRespCode());
					resp.setIdError(-1);
					resp.setMensajeError(response.getConsultaResult().getMensajeCajero());
					resp.setMensajeAntad(response.getConsultaResult().getMensajeCajero());
				}
			} else {
				resp.setRespCode("Error con el proveedor");
				resp.setIdError(-1);
			}
			
			json = jsonUtils.objectToJson(resp);
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL REALIZAR LA AUTORIZACION CON ANTAD: "+e.getCause());
			e.printStackTrace();
			json = "";
		} finally {
			LOGGER.info("RESPUESTA DE AUTORIZACION ANTAD: "+json);
			json = procesaRespuesta(json);
		}
		return json;
	}

	
	private double calculaDivisa(AntadResponse resp, String string) {
		double dolar = 0;
		ConsultaDivisaBsns divisa = new ConsultaDivisaBsns();
//		CompactData data = null;
		try {
//			data = (CompactData) webServiceBanxico.marshalSendAndReceive("http://www.banxico.org.mx/DgieWSWeb/DgieWS", new Object());
			dolar = divisa.calculaValorDolar(resp, Double.valueOf(string));
			if(dolar == 0) {
				dolar = dao.obtenDivisa("1");
			} else {
				dao.updateDivisa(dolar, "1");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dolar;
	}

	@SuppressWarnings("unused")
	private void insertaAntadDetalle(AntadInfo antadInfo) {
		LOGGER.info("INSERTANDO DETALLE ANTAD PARA LA TRANSACCION: "+antadInfo.getIdTransaccion());
		AntadDetalle antadDetalle = new AntadDetalle();
		try {
			antadDetalle.setIdTransaccion(antadInfo.getIdTransaccion());
			antadDetalle.setDescripcion(antadInfo.getConcepto()==null?"Consulta de saldo":antadInfo.getConcepto());
			antadDetalle.setTipoTarjeta(antadInfo.getTipoTarjeta());
			antadDetalle.setTotal(antadInfo.getMonto());
			antadDetalle.setComision(antadInfo.getComision());
			antadDetalle.setReferencia(antadInfo.getReferencia());
			antadDetalle.setEmisor(antadInfo.getEmisor());
			antadDetalle.setCodigoRespuesta("");
			antadDetalle.setFolioTransaccion(antadInfo.getFolioTransaccion());
			dao.insertaAntadDetalle(antadDetalle);		
			LOGGER.info("DETALLE ANTAD GUARDADO EXITOSAMENTE - ID TRANSACCION: "+antadInfo.getIdTransaccion());
		} catch (Exception e) {
			LOGGER.info("ERRRO AL GUARDAR DETALLE ANTAD - ID TRANSACCION: "
					+antadInfo.getIdTransaccion()+" - CAUSA: "+e.getCause());
		}
	}

	private void actualizaAntadDetalle(AntadResponse response,
			AntadInfo antadInfo) {
		AntadDetalle antadDetalle = new AntadDetalle();
		try {
			antadDetalle.setIdTransaccion(antadInfo.getIdTransaccion());
			antadDetalle.setIdTransaccionAntad(response.getFolioTransaccion() != null ? response.getFolioTransaccion() : "");
			antadDetalle.setNumAutorizacion(response.getNumAuth());
			antadDetalle.setCodigoRespuesta(response.getRespCode() != null ? response.getRespCode(): "00");
			dao.actualizaAntadDetalle(antadDetalle);
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL ACTUALIZAR LA TRANSACCION CON ANTAD: "+e.getStackTrace());
		}
	}

	@SuppressWarnings("unused")
	private Usuario getUsuario(long idUsuario) {
		LOGGER.info("BUSCANDO DATOS DE USUARIO - ID USUARIO: "+idUsuario);
		Usuario usuario = null;
		try {
			usuario = dao.getUsuarioInfo(idUsuario);
			if(usuario != null){
				LOGGER.info("USUARIO ENCONTRADO - ID USUARIO: "+idUsuario);
			} else {
				LOGGER.error("USUARIO NO ENCONTRADO - ID USUARIO: "+idUsuario);
			}
		} catch (Exception e) {
			LOGGER.error("ERROR AL BUSCAR LOS DATOS DE USUARIO - ID USUARIO: "+idUsuario);
			e.printStackTrace();
		}
		return usuario;
	}
	
	@SuppressWarnings("unused")
	private double obtenComision(int idProveedor) {
		LOGGER.info("OBTENIENDO COMISION PARA EL PRODUCTO: "+idProveedor);
		double comision = 0;
		try {
			comision = dao.obtenComision(idProveedor);
			LOGGER.info("COMISION EXITOSA PARA EL PRODUCTO: "+idProveedor);
		} catch (Exception e) {
			LOGGER.error("ERROR AL CONSULTA LA COMISION PARA EL PRODUCTO: "+idProveedor);
			e.printStackTrace();
		}
		return comision;
	}
	
	@SuppressWarnings("unused")
	private void actualizaBitacora(AutorizacionResponse response,
			AntadInfo antadInfo) {
		Bitacora bitacora = new Bitacora();
		try {
			bitacora.setIdTransaccion(antadInfo.getIdTransaccion());
			bitacora.setNoAutorizacion(response.getAutorizacionResult().getNumAuth());
			//bitacora.setCodigoError(response.getAutorizacionResult().getRespCode());
			dao.actualizaTransaccion(bitacora);
		} catch (Exception e) {
			LOGGER.error("OCURRIO UN ERROR AL ACTUALIZAR LA TRANSACCION CON ANTAD: "+e.getStackTrace());
		}
	}
	
	@SuppressWarnings("unused")
	private long insertaTransaccion(AntadInfo antadInfo, Usuario usuario) {
		LOGGER.info("INSERTANDO EN BITACORA LA TRANSACCION PARA EL USUARIO - ID USUARIO: "+antadInfo.getIdUsuario());
		Bitacora bitacora = new Bitacora();
		long idTransaccion = 0; 
		try {
			bitacora.setIdUsuario(antadInfo.getIdUsuario());
			bitacora.setIdProveedor(antadInfo.getIdProveedor());		
			bitacora.setIdProducto(antadInfo.getIdProducto());
			bitacora.setFecha("");
			bitacora.setHora("");
			bitacora.setConcepto(antadInfo.getConcepto());
			bitacora.setCargo(antadInfo.getCargo());
			bitacora.setTicket("");
			bitacora.setNoAutorizacion("");
			bitacora.setCodigoError("0");
			bitacora.setCardId(0);
			bitacora.setStatus(0);
			bitacora.setImei(antadInfo.getImei());
//			bitacora.setTarjetaCompra(AddcelCryptoRSA.encryptPublic(usuario.getTarjeta()));
			bitacora.setTarjetaCompra(usuario.getTarjeta());
			bitacora.setTipo(antadInfo.getTipo());
			bitacora.setSoftware(antadInfo.getSoftware());
			bitacora.setModelo(antadInfo.getModelo());
			bitacora.setWkey(antadInfo.getWkey());
			bitacora.setPase(0);
			idTransaccion = dao.insertaTransaccion(bitacora);
			LOGGER.info("BITACORA GUARDADA EXITOSAMENTE PARA EL USUARIO - ID USUARIO: "+antadInfo.getIdUsuario()
					+" - ID BITACORA: "+bitacora.getIdTransaccion());
		} catch (Exception e) {
			LOGGER.info("ERRRO AL GUARDAR LA BITACORA PARA EL USUARIO - ID USUARIO: "+antadInfo.getIdUsuario());
			e.printStackTrace();
		}
		return idTransaccion;
	}

//	public WebServiceTemplate getWebServiceBanxico() {
//		return webServiceBanxico;
//	}
//
//	public void setWebServiceBanxico(WebServiceTemplate webServiceBanxico) {
//		this.webServiceBanxico = webServiceBanxico;
//	}
	
	private static String procesaRespuesta(String json){
		return AddcelCrypto.encryptSensitive(SD.format(new Date()),json);
	}
	
	private static String procesaEntrada(String json){
		return AddcelCrypto.decryptSensitive(json);
	}

	public WebServiceTemplate getWebServiceTemplate() {
		return webServiceTemplate;
	}

	public void setWebServiceTemplate(WebServiceTemplate webServiceTemplate) {
		this.webServiceTemplate = webServiceTemplate;
	}
	
	public Double formatearDecimales(double numero, Integer numeroDecimales) {
		return Math.round(numero * Math.pow(10, numeroDecimales)) / Math.pow(10, numeroDecimales);
	}
}