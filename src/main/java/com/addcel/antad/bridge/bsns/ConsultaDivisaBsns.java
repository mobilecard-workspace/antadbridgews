package com.addcel.antad.bridge.bsns;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.antad.bridge.client.banxico.DgieWSPortProxy;
import com.addcel.antad.bridge.vo.AntadResponse;
import com.addcel.antad.bridge.vo.CompactData;
import com.addcel.antad.bridge.vo.DataSet;
import com.addcel.antad.bridge.vo.Header;
import com.addcel.antad.bridge.vo.Obs;
import com.addcel.antad.bridge.vo.ObsConverter;
import com.addcel.antad.bridge.vo.Sender;
import com.addcel.antad.bridge.vo.Series;
import com.thoughtworks.xstream.XStream;

public class ConsultaDivisaBsns {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConsultaDivisaBsns.class);
	
	private static DgieWSPortProxy proxy = null;
	
	private static XStream xStream;
	
	static {
		proxy = new DgieWSPortProxy();
	}
	
	private static XStream get() {
        if (xStream == null) {
            xStream = new XStream();
            xStream.alias("CompactData", CompactData.class);
        }
        return xStream;
    }
	
	public double calculaValorDolar(AntadResponse resp, double monto){
		double valorDolar = 0;
		try {
			
			valorDolar = consultaDivisa();
			valorDolar = valorDolar - (valorDolar * 0.015);
			resp.setTipoCambio(valorDolar);
			double montoUsd = resp.getMontoMxn() / valorDolar;
			double comisionUsd = montoUsd * 0.05;
			resp.setMontoUsd(montoUsd);
			resp.setComisionUsd(comisionUsd);
			LOGGER.info("VALOR DOLAR: "+valorDolar);
			LOGGER.info("MONTO DOLARES: "+montoUsd);
			LOGGER.info("COMISION DOLARES: "+comisionUsd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	private double consultaDivisa(){
		double dolar = 0;
		String result = null;
		CompactData data = null;
		try {
			result = proxy.tiposDeCambioBanxico();
			LOGGER.info("Resultado de la consulta: "+result);
			result = result.replace("bm:DataSet", "DataSet");
			result = result.replace("bm:SiblingGroup", "SiblingGroup");
			result = result.replace("bm:Series", "Series");
			result = result.replace("bm:Obs", "Obs");
//			LOGGER.info("Resultado de la consulta: "+result);
//			JAXBContext jaxbContext = JAXBContext.newInstance(CompactData.class);
//			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//			InputStream in = IOUtils.toInputStream(result, "UTF-8");
//			data = (CompactData) jaxbUnmarshaller.unmarshal(in);
			xStream = new XStream();
			
			xStream.alias("CompactData", CompactData.class);
			xStream.alias("Header", Header.class); 
			xStream.alias("DataSet", DataSet.class); 
			xStream.alias("Sender", Sender.class); 
			xStream.alias("Series", Series.class);  
			xStream.addImplicitCollection(DataSet.class, "Series", Series.class);
			xStream.alias("Obs", Obs.class); 

			xStream.useAttributeFor(Obs.class, "timePeriod");
			xStream.useAttributeFor(Obs.class, "obsValue");
			xStream.registerConverter(new ObsConverter());
			
//			xStream.aliasAttribute(Obs.class, "timePeriod", "TIME_PERIOD");
//			xStream.aliasAttribute(Obs.class, "obsValue", "OBS_VALUE");
//			xStream.aliasField("TIME_PERIOD", Obs.class, "TIME_PERIOD");
//			xStream.aliasField("OBS_VALUE", Obs.class, "OBS_VALUE");
//			xStream.aliasField("TIME_PERIOD", Obs.class, "TIME_PERIOD");
//			xStream.aliasField("OBS_VALUE", Obs.class, "OBS_VALUE");
//			xStream.registerConverter(new Key1SingleValueConverter());
			
			data = (CompactData) get().fromXML(result);
			
			ArrayList<Series> series = data.getDataSet().getSeries();
			Series serie = series.get(1);
			dolar = Double.valueOf(serie.getObs().getObsValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dolar;
	}
	
}
