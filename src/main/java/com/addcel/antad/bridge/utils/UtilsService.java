package com.addcel.antad.bridge.utils;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UtilsService {
	private static final Logger logger = LoggerFactory.getLogger(UtilsService.class);
	
	private static final String patron = "dd/MM/yyyy";
	private static final String patron_large = "yyyy/MM/dd HH:mm:ss";
	private static final String PATRON_LARGE_2 = "yyyy-MM-dd'T'HH:mm:ss";
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
    private static final SimpleDateFormat formato_large = new SimpleDateFormat(patron_large, new Locale("ES", "co"));
    private static final SimpleDateFormat FORMATO_LARGE_2 = new SimpleDateFormat(PATRON_LARGE_2, new Locale("ES", "co"));
    
    private static final String patronImp = "########0.00";
	private static final String patronImpMon = "###,###,##0.00";
	private static DecimalFormat formatoDec;
	private static DecimalFormat formatoMon;
	private static DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
	private static DecimalFormatSymbols simbolosMon = new DecimalFormatSymbols();
	
	private static Formatter fmt= new Formatter();
	
	static{
		simbolos.setDecimalSeparator('.');
		simbolosMon.setDecimalSeparator('.');
		simbolosMon.setGroupingSeparator(',');
		
		formatoDec = new DecimalFormat(patronImp,simbolos);
		formatoMon = new DecimalFormat(patronImpMon,simbolosMon);
	}
	
	public static Date getFechaExpDate(String fechaExp){
		Date fecha = null;
		String[] fechaArr = null; 
		try{
			fechaArr = fechaExp.split("/");
			logger.info("Calculando fecha Expiracion: " + fechaExp);
			fecha = formato.parse(new StringBuffer().append("01/").append(fechaArr[0])
					//.append("/20")
					.append("/").append(fechaArr[1]).toString());
		}catch(Exception e){
			logger.error("Error en Fecha Expiracion: {}",e );
		}
		return fecha;
	}
	
	public static String getFechaExpDate(String formato, String fechaExp){
		String fecha = null;
		String[] fechaArr = null; 
		Date date = null;
		SimpleDateFormat sdf = null;
		SimpleDateFormat sdf2 = null;
		try{
			fechaArr = fechaExp.split("/");
			logger.info("Calculando fecha Expiracion: " + fechaExp);
			sdf = new SimpleDateFormat(formato, new Locale("ES", "co"));
			sdf2 = new SimpleDateFormat("yyyy-MM-dd", new Locale("ES", "co"));
			date = sdf.parse(new StringBuffer().append("01/").append(fechaArr[0]).append("/").append(fechaArr[1]).toString());
			fecha = sdf2.format(date);
		}catch(Exception e){
			logger.error("Error en Fecha Expiracion: {}",e );
		}
		return fecha;
	}
	
	public static String getFechaLarga(Calendar fecha){
		String resp = null;
		try{
			resp = formato_large.format(fecha.getTime());
		}catch(Exception e){
			logger.error("Error en getFechaLarga: {}",e );
		}
		return resp;
	}

	public static String getFechaLarga2(Calendar fecha){
		String resp = null;
		try{
			resp = FORMATO_LARGE_2.format(fecha.getTime());
		}catch(Exception e){
			logger.error("Error en getFechaLarga: {}",e );
			e.printStackTrace();
		}
		return resp;
	}
	
	public static String getFechaActual(){
		String resp = null;
		try{
			resp = formato_large.format(new Date());
		}catch(Exception e){
			logger.error("Error en getFechaActual: {}",e );
		}
		return resp;
	}
    
	public static String getFechaActual(String formato){
		SimpleDateFormat sdf = null;
		String resp = null;
		try{
			sdf = new SimpleDateFormat(formato, new Locale("ES", "co"));
			resp = sdf.format(new Date());
		}catch(Exception e){
			logger.error("Error en getFechaActual: {}",e );
		}
		return resp;
	}
	
	public static String getFechaActual(String formato, String fecha){
		SimpleDateFormat sdf = null;
		String resp = null;
		try{
			sdf = new SimpleDateFormat(formato, new Locale("ES", "co"));
			resp = sdf.format(new Date());
		}catch(Exception e){
			logger.error("Error en getFechaActual: {}",e );
		}
		return resp;
	}
	
	public static String getFechaActualCorta(){
		String resp = null;
		Calendar cal = Calendar.getInstance();
		try{
			Date date = cal.getTime();
			resp = formato.format(date);
		}catch(Exception e){
			logger.error("Error en getFechaActual: {}",e );
		}
		return resp;
	}
	
	
	public static synchronized int diferenciasDeFechas(Date fechaInicial, Date fechaFinal) {

        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        String fechaInicioString = df.format(fechaInicial);
        try {
            fechaInicial = df.parse(fechaInicioString);
        } catch (ParseException ex) {
        }

        String fechaFinalString = df.format(fechaFinal);
        try {
            fechaFinal = df.parse(fechaFinalString);
        } catch (ParseException ex) {
        }

        long fechaInicialMs = fechaInicial.getTime();
        long fechaFinalMs = fechaFinal.getTime();
        long diferencia = fechaFinalMs - fechaInicialMs;
        double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
        return ((int) dias);
    }
	
	
	public static boolean isEmpty(String cadena){
		boolean resp = false;
		if(cadena == null){
			resp = true;
		}else if("".equals(cadena)){
			resp = true;
		}
		return resp; 
		
	}
	
	public static String formatoImporteMon(BigDecimal numDecimal){
		if(numDecimal == null){
			return "0.00";
		}
		return formatoImporteMon(numDecimal.doubleValue());
	}
	
	public static String formatoImporteMon(String numString){
		String total = null;
		if(!isEmpty(numString)){
			total = formatoImporteMon(Double.parseDouble(numString));
		}
		return total;
	}
	
	public static String formatoImporteMon(double numDouble){
		String total = null;
		try{
			total = formatoMon.format(numDouble);
		}catch(Exception e){
			logger.error("Error en formatoImporteMon: "+ e.getMessage());
		}
		return total;
	}

}