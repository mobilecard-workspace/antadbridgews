/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.antad.bridge.utils;

/**
 *
 * @author JOCAMPO
 */
public enum ServiceErrorEnumeration {

    RFCnoValido("1","El RFC no es valido"),
    metodo("2","Uno Dos");
    
    String codigo;
    String error;

    private ServiceErrorEnumeration(String codigo, String error) {
        this.codigo = codigo;
        this.error = error;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getError() {
        return error;
    }

}
