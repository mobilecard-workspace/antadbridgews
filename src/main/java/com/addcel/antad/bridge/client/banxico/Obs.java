package com.addcel.antad.bridge.client.banxico;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
		"TIME_PERIOD",
		"OBS_VALUE"
})
public class Obs {

//	@XmlElement(name = "TIME_PERIOD")
	private String TIME_PERIOD;
	
//	@XmlElement(name = "OBS_VALUE")
	private String OBS_VALUE;

	public String getTIME_PERIOD() {
		return TIME_PERIOD;
	}

	public void setTIME_PERIOD(String tIME_PERIOD) {
		TIME_PERIOD = tIME_PERIOD;
	}

	public String getOBS_VALUE() {
		return OBS_VALUE;
	}

	public void setOBS_VALUE(String oBS_VALUE) {
		OBS_VALUE = oBS_VALUE;
	}
	
}
