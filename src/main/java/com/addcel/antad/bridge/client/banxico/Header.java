package com.addcel.antad.bridge.client.banxico;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Header", propOrder = {
    "ID",
    "Test",
    "Truncated",
    "Name",
    "Prepared",
    "Sender",
    "DataSetAction",
    "Extracted"
})
public class Header {

//	@XmlElement(name = "ID")
	private String ID;
	
//	@XmlElement(name = "Test")
	private String Test;
	
//	@XmlElement(name = "Truncated")
	private String Truncated;
	
//	@XmlElement(name = "Name")
	private String Name;
	
//	@XmlElement(name = "Prepared")
	private String Prepared;
	
//	@XmlElement(name = "Sender")
	private Sender Sender;
	
//	@XmlElement(name = "DataSetAction")
	private String DataSetAction;
	
//	@XmlElement(name = "Extracted")
	private String Extracted;

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getTest() {
		return Test;
	}

	public void setTest(String test) {
		Test = test;
	}

	public String getTruncated() {
		return Truncated;
	}

	public void setTruncated(String truncated) {
		Truncated = truncated;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getPrepared() {
		return Prepared;
	}

	public void setPrepared(String prepared) {
		Prepared = prepared;
	}

	public Sender getSender() {
		return Sender;
	}

	public void setSender(Sender sender) {
		Sender = sender;
	}

	public String getDataSetAction() {
		return DataSetAction;
	}

	public void setDataSetAction(String dataSetAction) {
		DataSetAction = dataSetAction;
	}

	public String getExtracted() {
		return Extracted;
	}

	public void setExtracted(String extracted) {
		Extracted = extracted;
	}

}
