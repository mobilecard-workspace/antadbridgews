package com.addcel.antad.bridge.client.banxico;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CompactData", propOrder = {
		"Header",
	    "DataSet"
})
@XmlRootElement(name = "CompactData")
public class CompactData {

	private Header Header;
	
	private DataSet DataSet;

	public Header getHeader() {
		return Header;
	}

	public void setHeader(Header header) {
		Header = header;
	}

	public DataSet getDataSet() {
		return DataSet;
	}

	public void setDataSet(DataSet dataSet) {
		DataSet = dataSet;
	}

}
