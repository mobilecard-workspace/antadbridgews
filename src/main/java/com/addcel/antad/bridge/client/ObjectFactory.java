//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.02.17 a las 09:23:40 PM CST 
//


package com.addcel.antad.bridge.client;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.addcel.mx.antad.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.addcel.mx.antad.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Autorizacion }
     * 
     */
    public Autorizacion createAutorizacion() {
        return new Autorizacion();
    }

    /**
     * Create an instance of {@link AutorizacionResponse }
     * 
     */
    public AutorizacionResponse createAutorizacionResponse() {
        return new AutorizacionResponse();
    }

    /**
     * Create an instance of {@link ANTADWSResponse }
     * 
     */
    public ANTADWSResponse createANTADWSResponse() {
        return new ANTADWSResponse();
    }

    /**
     * Create an instance of {@link AutorizacionconCertificado }
     * 
     */
    public AutorizacionconCertificado createAutorizacionconCertificado() {
        return new AutorizacionconCertificado();
    }

    /**
     * Create an instance of {@link AutorizacionconCertificadoResponse }
     * 
     */
    public AutorizacionconCertificadoResponse createAutorizacionconCertificadoResponse() {
        return new AutorizacionconCertificadoResponse();
    }

    /**
     * Create an instance of {@link AutorizacionFAC }
     * 
     */
    public AutorizacionFAC createAutorizacionFAC() {
        return new AutorizacionFAC();
    }

    /**
     * Create an instance of {@link AutorizacionFACResponse }
     * 
     */
    public AutorizacionFACResponse createAutorizacionFACResponse() {
        return new AutorizacionFACResponse();
    }

    /**
     * Create an instance of {@link Consulta }
     * 
     */
    public Consulta createConsulta() {
        return new Consulta();
    }

    /**
     * Create an instance of {@link ConsultaResponse }
     * 
     */
    public ConsultaResponse createConsultaResponse() {
        return new ConsultaResponse();
    }

}
