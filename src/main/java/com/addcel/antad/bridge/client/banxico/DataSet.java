package com.addcel.antad.bridge.client.banxico;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataSet", propOrder = {
		"SiblingGroup",
	    "Series"
})
public class DataSet {

//	@XmlElement(name = "Series")
	private Series[] series;
	
//	@XmlElement(name = "SiblingGroup")
	private String siblingGroup;

	public Series[] getSeries() {
		return series;
	}

	public void setSeries(Series[] series) {
		this.series = series;
	}

	public String getSiblingGroup() {
		return siblingGroup;
	}

	public void setSiblingGroup(String siblingGroup) {
		this.siblingGroup = siblingGroup;
	}
}
