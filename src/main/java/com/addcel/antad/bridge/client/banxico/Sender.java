package com.addcel.antad.bridge.client.banxico;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Sender", propOrder = {
    "Name",
    "Contact"
})
public class Sender {

//	@XmlElement(name = "Name")
	private String Name;
	
//	@XmlElement(name = "Contact")
	private Contact Contact;

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public Contact getContact() {
		return Contact;
	}

	public void setContact(Contact contact) {
		Contact = contact;
	}


}
