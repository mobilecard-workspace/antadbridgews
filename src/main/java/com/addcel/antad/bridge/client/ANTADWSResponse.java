//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.11 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.02.17 a las 09:23:40 PM CST 
//


package com.addcel.antad.bridge.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ANTADWSResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ANTADWSResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RespCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumAuth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MensajeTicket" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MensajeCajero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FolioTransaccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FolioComercio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DatosAdicionales" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ANTADWSResponse", propOrder = {
    "respCode",
    "numAuth",
    "mensajeTicket",
    "mensajeCajero",
    "folioTransaccion",
    "folioComercio",
    "datosAdicionales"
})
public class ANTADWSResponse {

    @XmlElement(name = "RespCode")
    protected String respCode;
    @XmlElement(name = "NumAuth")
    protected String numAuth;
    @XmlElement(name = "MensajeTicket")
    protected String mensajeTicket;
    @XmlElement(name = "MensajeCajero")
    protected String mensajeCajero;
    @XmlElement(name = "FolioTransaccion")
    protected String folioTransaccion;
    @XmlElement(name = "FolioComercio")
    protected String folioComercio;
    @XmlElement(name = "DatosAdicionales")
    protected String datosAdicionales;

    /**
     * Obtiene el valor de la propiedad respCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRespCode() {
        return respCode;
    }

    /**
     * Define el valor de la propiedad respCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRespCode(String value) {
        this.respCode = value;
    }

    /**
     * Obtiene el valor de la propiedad numAuth.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumAuth() {
        return numAuth;
    }

    /**
     * Define el valor de la propiedad numAuth.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumAuth(String value) {
        this.numAuth = value;
    }

    /**
     * Obtiene el valor de la propiedad mensajeTicket.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensajeTicket() {
        return mensajeTicket;
    }

    /**
     * Define el valor de la propiedad mensajeTicket.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensajeTicket(String value) {
        this.mensajeTicket = value;
    }

    /**
     * Obtiene el valor de la propiedad mensajeCajero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensajeCajero() {
        return mensajeCajero;
    }

    /**
     * Define el valor de la propiedad mensajeCajero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensajeCajero(String value) {
        this.mensajeCajero = value;
    }

    /**
     * Obtiene el valor de la propiedad folioTransaccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioTransaccion() {
        return folioTransaccion;
    }

    /**
     * Define el valor de la propiedad folioTransaccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioTransaccion(String value) {
        this.folioTransaccion = value;
    }

    /**
     * Obtiene el valor de la propiedad folioComercio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioComercio() {
        return folioComercio;
    }

    /**
     * Define el valor de la propiedad folioComercio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioComercio(String value) {
        this.folioComercio = value;
    }

    /**
     * Obtiene el valor de la propiedad datosAdicionales.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatosAdicionales() {
        return datosAdicionales;
    }

    /**
     * Define el valor de la propiedad datosAdicionales.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatosAdicionales(String value) {
        this.datosAdicionales = value;
    }

}
