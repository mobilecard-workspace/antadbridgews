package com.addcel.antad.bridge.webservice;

import java.io.Serializable;
import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.addcel.antad.bridge.client.Autorizacion;
import com.addcel.antad.bridge.client.AutorizacionResponse;
import com.addcel.antad.bridge.client.Consulta;
import com.addcel.antad.bridge.client.ConsultaResponse;
import com.addcel.antad.bridge.client.ObjectFactory;

@Component
public class AntadWSTemplate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AntadWSTemplate.class);
	
	@Autowired
    @Qualifier("webServiceTemplateAntad")
    private transient WebServiceTemplate webServiceTemplate;
	
	public AutorizacionResponse autorizacion(String referencia, String emisor, double monto, double comision, long idTransaccion){
		LOGGER.info("*** INICIANDO WS CLIENT PAGOS SERVICIOS ANTAD **** ");
		AutorizacionResponse response = null;
		Autorizacion requestWS = null;
		ObjectFactory factory = new ObjectFactory();
		try {
			requestWS = factory.createAutorizacion();
			requestWS.setComercio(8315);
			requestWS.setSucursal("1");
			requestWS.setCaja("1");
			requestWS.setTicket("1");
			requestWS.setFolioComercio(idTransaccion);
			requestWS.setOperacion("000031");
			requestWS.setReferencia(referencia);
			requestWS.setMonto(monto);
			requestWS.setEmisor(emisor);
			requestWS.setModoIngreso("022");
			requestWS.setComision(new BigDecimal(comision));
			response = (AutorizacionResponse) webServiceTemplate.marshalSendAndReceive(requestWS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.info("*** TERMINA WS CLIENT CONSULTA ANTAD **** ");
		return response;
	}
	
	
	public ConsultaResponse consultaClientAntad(String referencia, String emisor, double comision){
		LOGGER.info("*** INICIANDO WS CLIENT CONSULTA ANTAD **** ");
		ConsultaResponse response = null;
		Consulta requestWS = null;		
		ObjectFactory factory = new ObjectFactory();
		try {
			requestWS = factory.createConsulta();
			requestWS.setComercio(8315);
			requestWS.setSucursal("1");
			requestWS.setCaja("1");
			requestWS.setTicket("1");
			requestWS.setFolioComercio(1l);
			requestWS.setOperacion("000030");
			requestWS.setReferencia(referencia);
			requestWS.setMonto(new BigDecimal(0));
			requestWS.setEmisor(emisor);
			requestWS.setModoIngreso("022");
			requestWS.setComision(new BigDecimal(comision));
			response = (ConsultaResponse) webServiceTemplate.marshalSendAndReceive(requestWS);
//			response = (ConsultaResponse) webServiceTemplate.marshalSendAndReceive(requestWS, new WebServiceMessageCallback() {
//			    public void doWithMessage(WebServiceMessage message) {
//			        ((SoapMessage)message).setSoapAction("http://tempuri.org/Consulta");
////			        ((SoapMessage)message).
//			    }
//			});
//			webServiceTemplate.sendSourceAndReceiveToResult("http://tempuri.org/Consulta", source, result);
			
//			webServiceTemplate.marshalSendAndReceive(requestWS, requestCallback)
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.info("*** TERMINA WS CLIENT CONSULTA ANTAD **** ");
		return response;
	}

	public WebServiceTemplate getWebServiceTemplate() {
		return webServiceTemplate;
	}

	public void setWebServiceTemplate(WebServiceTemplate webServiceTemplate) {
		this.webServiceTemplate = webServiceTemplate;
	}
	
}
