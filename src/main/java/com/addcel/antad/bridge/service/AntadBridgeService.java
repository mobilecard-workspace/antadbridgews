/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.antad.bridge.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 *
 * @author JOCAMPO
 */
@WebService(name= "Services")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface AntadBridgeService {
    
    /**
     * Web service operation
     * @param autentica
     * @param usuario
     * @return 
     */
    @WebMethod(operationName = "procesaAutorizacion")
    public String autorizacion(@WebParam(name = "json") String json);
    
    
    /**
     * Web service operation
     * @param autentica
     * @param usuario
     * @return 
     */
    @WebMethod(operationName = "consulta")
    public String consultaSaldo(@WebParam(name = "json") String json);
    
    /**
     * Web service operation
     * @param autentica
     * @param usuario
     * @return 
     */
    @WebMethod(operationName = "registro")
    public String registro(@WebParam(name = "json") String json);
       
}