/**
 * 
 */
package com.addcel.antad.bridge.ibatis;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author JOCAMPO
 * 
 */

public class SpringFactory {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SpringFactory.class);

	private static final String resource = "com/addcel/services/ws/spring/root-context.xml";
	
	private static ClassPathXmlApplicationContext ctxt;

	public static ClassPathXmlApplicationContext getApplicationContexInstance() {
		try {
			if(ctxt==null){
	            LOGGER.debug("[ANTAD BRIDGE WS] **** SE HA CARGADO EL CONTEXTO DE SPRING ***");
	            ctxt = new ClassPathXmlApplicationContext(resource);
	        }else{
	        	LOGGER.debug("YA EXISTE CONTEXTO");
	        }
		} catch (Exception e) {
			LOGGER.error("Error al tratar de crear la instancia SqlSessionFActoryImpl.",e);
		}
		return ctxt;
	}

	public static ClassPathXmlApplicationContext reloadApplicationContex() {
		try {
			ctxt = new ClassPathXmlApplicationContext(resource);
		} catch (Exception e) {
			LOGGER.error("Error al tratar de crear la instancia SqlSessionFActoryImpl.",e);
		}
		return ctxt;
	}

}
