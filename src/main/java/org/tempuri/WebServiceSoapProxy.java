package org.tempuri;

public class WebServiceSoapProxy implements org.tempuri.WebServiceSoap {
  private String _endpoint = null;
  private org.tempuri.WebServiceSoap webServiceSoap = null;
  
  public WebServiceSoapProxy() {
    _initWebServiceSoapProxy();
  }
  
  public WebServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initWebServiceSoapProxy();
  }
  
  private void _initWebServiceSoapProxy() {
    try {
      webServiceSoap = (new org.tempuri.WebServiceLocator()).getWebServiceSoap();
      if (webServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)webServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)webServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (webServiceSoap != null)
      ((javax.xml.rpc.Stub)webServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.WebServiceSoap getWebServiceSoap() {
    if (webServiceSoap == null)
      _initWebServiceSoapProxy();
    return webServiceSoap;
  }
  
  public org.tempuri.ANTADWSResponse autorizacion(int comercio, java.lang.String sucursal, java.lang.String caja, java.lang.String cajero, java.lang.String horario, java.lang.String ticket, long folioComercio, java.lang.String operacion, java.lang.String referencia, java.math.BigDecimal monto, java.lang.String emisor, java.lang.String modoIngreso, java.math.BigDecimal comision, java.lang.String referencia2, java.lang.String referencia3, int reintento, java.lang.String FAC) throws java.rmi.RemoteException{
    if (webServiceSoap == null)
      _initWebServiceSoapProxy();
    return webServiceSoap.autorizacion(comercio, sucursal, caja, cajero, horario, ticket, folioComercio, operacion, referencia, monto, emisor, modoIngreso, comision, referencia2, referencia3, reintento, FAC);
  }
  
  public org.tempuri.ANTADWSResponse consulta(int comercio, java.lang.String sucursal, java.lang.String caja, java.lang.String cajero, java.lang.String horario, java.lang.String ticket, long folioComercio, java.lang.String operacion, java.lang.String referencia, java.math.BigDecimal monto, java.lang.String emisor, java.lang.String modoIngreso, java.math.BigDecimal comision, java.lang.String referencia2, java.lang.String referencia3, int reintento, java.lang.String FAC) throws java.rmi.RemoteException{
    if (webServiceSoap == null)
      _initWebServiceSoapProxy();
    return webServiceSoap.consulta(comercio, sucursal, caja, cajero, horario, ticket, folioComercio, operacion, referencia, monto, emisor, modoIngreso, comision, referencia2, referencia3, reintento, FAC);
  }
  
  public org.tempuri.RegistrosResponse registro(java.lang.String comercio, java.lang.String sucursal, java.lang.String macAddress) throws java.rmi.RemoteException{
    if (webServiceSoap == null)
      _initWebServiceSoapProxy();
    return webServiceSoap.registro(comercio, sucursal, macAddress);
  }
  
  public org.tempuri.ConsultarEstatusResponse consultaEstatus(int comercio, java.lang.String sucursal, java.lang.String caja, java.lang.String cajero, java.lang.String horario, java.lang.String ticket, long folioComercio, java.lang.String operacion, java.lang.String referencia, java.math.BigDecimal monto, java.lang.String emisor, java.lang.String modoIngreso, java.math.BigDecimal comision, java.lang.String SKU, java.lang.String referencia2, java.lang.String referencia3) throws java.rmi.RemoteException{
    if (webServiceSoap == null)
      _initWebServiceSoapProxy();
    return webServiceSoap.consultaEstatus(comercio, sucursal, caja, cajero, horario, ticket, folioComercio, operacion, referencia, monto, emisor, modoIngreso, comision, SKU, referencia2, referencia3);
  }
  
  public org.tempuri.ANTADWSResponse reverso(int comercio, java.lang.String sucursal, java.lang.String caja, java.lang.String cajero, java.lang.String horario, java.lang.String ticket, long folioComercio, java.lang.String operacion, java.lang.String referencia, java.math.BigDecimal monto, java.lang.String emisor, java.lang.String modoIngreso, java.math.BigDecimal comision, java.lang.String referencia2, java.lang.String referencia3, int reintento, java.lang.String fechaContab) throws java.rmi.RemoteException{
    if (webServiceSoap == null)
      _initWebServiceSoapProxy();
    return webServiceSoap.reverso(comercio, sucursal, caja, cajero, horario, ticket, folioComercio, operacion, referencia, monto, emisor, modoIngreso, comision, referencia2, referencia3, reintento, fechaContab);
  }
  
  
}