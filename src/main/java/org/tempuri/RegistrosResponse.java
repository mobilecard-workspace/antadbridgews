/**
 * RegistrosResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class RegistrosResponse  implements java.io.Serializable {
    private java.lang.String respCode;

    private java.lang.String comercio;

    private java.lang.String sucursal;

    private java.lang.String claveAcceso;

    private java.lang.String token;

    private java.lang.String mensajeCajero;

    public RegistrosResponse() {
    }

    public RegistrosResponse(
           java.lang.String respCode,
           java.lang.String comercio,
           java.lang.String sucursal,
           java.lang.String claveAcceso,
           java.lang.String token,
           java.lang.String mensajeCajero) {
           this.respCode = respCode;
           this.comercio = comercio;
           this.sucursal = sucursal;
           this.claveAcceso = claveAcceso;
           this.token = token;
           this.mensajeCajero = mensajeCajero;
    }


    /**
     * Gets the respCode value for this RegistrosResponse.
     * 
     * @return respCode
     */
    public java.lang.String getRespCode() {
        return respCode;
    }


    /**
     * Sets the respCode value for this RegistrosResponse.
     * 
     * @param respCode
     */
    public void setRespCode(java.lang.String respCode) {
        this.respCode = respCode;
    }


    /**
     * Gets the comercio value for this RegistrosResponse.
     * 
     * @return comercio
     */
    public java.lang.String getComercio() {
        return comercio;
    }


    /**
     * Sets the comercio value for this RegistrosResponse.
     * 
     * @param comercio
     */
    public void setComercio(java.lang.String comercio) {
        this.comercio = comercio;
    }


    /**
     * Gets the sucursal value for this RegistrosResponse.
     * 
     * @return sucursal
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }


    /**
     * Sets the sucursal value for this RegistrosResponse.
     * 
     * @param sucursal
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }


    /**
     * Gets the claveAcceso value for this RegistrosResponse.
     * 
     * @return claveAcceso
     */
    public java.lang.String getClaveAcceso() {
        return claveAcceso;
    }


    /**
     * Sets the claveAcceso value for this RegistrosResponse.
     * 
     * @param claveAcceso
     */
    public void setClaveAcceso(java.lang.String claveAcceso) {
        this.claveAcceso = claveAcceso;
    }


    /**
     * Gets the token value for this RegistrosResponse.
     * 
     * @return token
     */
    public java.lang.String getToken() {
        return token;
    }


    /**
     * Sets the token value for this RegistrosResponse.
     * 
     * @param token
     */
    public void setToken(java.lang.String token) {
        this.token = token;
    }


    /**
     * Gets the mensajeCajero value for this RegistrosResponse.
     * 
     * @return mensajeCajero
     */
    public java.lang.String getMensajeCajero() {
        return mensajeCajero;
    }


    /**
     * Sets the mensajeCajero value for this RegistrosResponse.
     * 
     * @param mensajeCajero
     */
    public void setMensajeCajero(java.lang.String mensajeCajero) {
        this.mensajeCajero = mensajeCajero;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RegistrosResponse)) return false;
        RegistrosResponse other = (RegistrosResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.respCode==null && other.getRespCode()==null) || 
             (this.respCode!=null &&
              this.respCode.equals(other.getRespCode()))) &&
            ((this.comercio==null && other.getComercio()==null) || 
             (this.comercio!=null &&
              this.comercio.equals(other.getComercio()))) &&
            ((this.sucursal==null && other.getSucursal()==null) || 
             (this.sucursal!=null &&
              this.sucursal.equals(other.getSucursal()))) &&
            ((this.claveAcceso==null && other.getClaveAcceso()==null) || 
             (this.claveAcceso!=null &&
              this.claveAcceso.equals(other.getClaveAcceso()))) &&
            ((this.token==null && other.getToken()==null) || 
             (this.token!=null &&
              this.token.equals(other.getToken()))) &&
            ((this.mensajeCajero==null && other.getMensajeCajero()==null) || 
             (this.mensajeCajero!=null &&
              this.mensajeCajero.equals(other.getMensajeCajero())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRespCode() != null) {
            _hashCode += getRespCode().hashCode();
        }
        if (getComercio() != null) {
            _hashCode += getComercio().hashCode();
        }
        if (getSucursal() != null) {
            _hashCode += getSucursal().hashCode();
        }
        if (getClaveAcceso() != null) {
            _hashCode += getClaveAcceso().hashCode();
        }
        if (getToken() != null) {
            _hashCode += getToken().hashCode();
        }
        if (getMensajeCajero() != null) {
            _hashCode += getMensajeCajero().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RegistrosResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "RegistrosResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("respCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "RespCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comercio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Comercio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sucursal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Sucursal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("claveAcceso");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ClaveAcceso"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("token");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Token"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensajeCajero");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "MensajeCajero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
