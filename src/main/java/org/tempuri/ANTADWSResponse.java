/**
 * ANTADWSResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class ANTADWSResponse  implements java.io.Serializable {
    private java.lang.String respCode;

    private java.lang.String numAuth;

    private java.lang.String mensajeTicket;

    private java.lang.String mensajeCajero;

    private java.lang.String folioTransaccion;

    private java.lang.String folioComercio;

    private java.lang.String datosAdicionales;

    private java.lang.String comision;

    public ANTADWSResponse() {
    }

    public ANTADWSResponse(
           java.lang.String respCode,
           java.lang.String numAuth,
           java.lang.String mensajeTicket,
           java.lang.String mensajeCajero,
           java.lang.String folioTransaccion,
           java.lang.String folioComercio,
           java.lang.String datosAdicionales,
           java.lang.String comision) {
           this.respCode = respCode;
           this.numAuth = numAuth;
           this.mensajeTicket = mensajeTicket;
           this.mensajeCajero = mensajeCajero;
           this.folioTransaccion = folioTransaccion;
           this.folioComercio = folioComercio;
           this.datosAdicionales = datosAdicionales;
           this.comision = comision;
    }


    /**
     * Gets the respCode value for this ANTADWSResponse.
     * 
     * @return respCode
     */
    public java.lang.String getRespCode() {
        return respCode;
    }


    /**
     * Sets the respCode value for this ANTADWSResponse.
     * 
     * @param respCode
     */
    public void setRespCode(java.lang.String respCode) {
        this.respCode = respCode;
    }


    /**
     * Gets the numAuth value for this ANTADWSResponse.
     * 
     * @return numAuth
     */
    public java.lang.String getNumAuth() {
        return numAuth;
    }


    /**
     * Sets the numAuth value for this ANTADWSResponse.
     * 
     * @param numAuth
     */
    public void setNumAuth(java.lang.String numAuth) {
        this.numAuth = numAuth;
    }


    /**
     * Gets the mensajeTicket value for this ANTADWSResponse.
     * 
     * @return mensajeTicket
     */
    public java.lang.String getMensajeTicket() {
        return mensajeTicket;
    }


    /**
     * Sets the mensajeTicket value for this ANTADWSResponse.
     * 
     * @param mensajeTicket
     */
    public void setMensajeTicket(java.lang.String mensajeTicket) {
        this.mensajeTicket = mensajeTicket;
    }


    /**
     * Gets the mensajeCajero value for this ANTADWSResponse.
     * 
     * @return mensajeCajero
     */
    public java.lang.String getMensajeCajero() {
        return mensajeCajero;
    }


    /**
     * Sets the mensajeCajero value for this ANTADWSResponse.
     * 
     * @param mensajeCajero
     */
    public void setMensajeCajero(java.lang.String mensajeCajero) {
        this.mensajeCajero = mensajeCajero;
    }


    /**
     * Gets the folioTransaccion value for this ANTADWSResponse.
     * 
     * @return folioTransaccion
     */
    public java.lang.String getFolioTransaccion() {
        return folioTransaccion;
    }


    /**
     * Sets the folioTransaccion value for this ANTADWSResponse.
     * 
     * @param folioTransaccion
     */
    public void setFolioTransaccion(java.lang.String folioTransaccion) {
        this.folioTransaccion = folioTransaccion;
    }


    /**
     * Gets the folioComercio value for this ANTADWSResponse.
     * 
     * @return folioComercio
     */
    public java.lang.String getFolioComercio() {
        return folioComercio;
    }


    /**
     * Sets the folioComercio value for this ANTADWSResponse.
     * 
     * @param folioComercio
     */
    public void setFolioComercio(java.lang.String folioComercio) {
        this.folioComercio = folioComercio;
    }


    /**
     * Gets the datosAdicionales value for this ANTADWSResponse.
     * 
     * @return datosAdicionales
     */
    public java.lang.String getDatosAdicionales() {
        return datosAdicionales;
    }


    /**
     * Sets the datosAdicionales value for this ANTADWSResponse.
     * 
     * @param datosAdicionales
     */
    public void setDatosAdicionales(java.lang.String datosAdicionales) {
        this.datosAdicionales = datosAdicionales;
    }


    /**
     * Gets the comision value for this ANTADWSResponse.
     * 
     * @return comision
     */
    public java.lang.String getComision() {
        return comision;
    }


    /**
     * Sets the comision value for this ANTADWSResponse.
     * 
     * @param comision
     */
    public void setComision(java.lang.String comision) {
        this.comision = comision;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ANTADWSResponse)) return false;
        ANTADWSResponse other = (ANTADWSResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.respCode==null && other.getRespCode()==null) || 
             (this.respCode!=null &&
              this.respCode.equals(other.getRespCode()))) &&
            ((this.numAuth==null && other.getNumAuth()==null) || 
             (this.numAuth!=null &&
              this.numAuth.equals(other.getNumAuth()))) &&
            ((this.mensajeTicket==null && other.getMensajeTicket()==null) || 
             (this.mensajeTicket!=null &&
              this.mensajeTicket.equals(other.getMensajeTicket()))) &&
            ((this.mensajeCajero==null && other.getMensajeCajero()==null) || 
             (this.mensajeCajero!=null &&
              this.mensajeCajero.equals(other.getMensajeCajero()))) &&
            ((this.folioTransaccion==null && other.getFolioTransaccion()==null) || 
             (this.folioTransaccion!=null &&
              this.folioTransaccion.equals(other.getFolioTransaccion()))) &&
            ((this.folioComercio==null && other.getFolioComercio()==null) || 
             (this.folioComercio!=null &&
              this.folioComercio.equals(other.getFolioComercio()))) &&
            ((this.datosAdicionales==null && other.getDatosAdicionales()==null) || 
             (this.datosAdicionales!=null &&
              this.datosAdicionales.equals(other.getDatosAdicionales()))) &&
            ((this.comision==null && other.getComision()==null) || 
             (this.comision!=null &&
              this.comision.equals(other.getComision())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRespCode() != null) {
            _hashCode += getRespCode().hashCode();
        }
        if (getNumAuth() != null) {
            _hashCode += getNumAuth().hashCode();
        }
        if (getMensajeTicket() != null) {
            _hashCode += getMensajeTicket().hashCode();
        }
        if (getMensajeCajero() != null) {
            _hashCode += getMensajeCajero().hashCode();
        }
        if (getFolioTransaccion() != null) {
            _hashCode += getFolioTransaccion().hashCode();
        }
        if (getFolioComercio() != null) {
            _hashCode += getFolioComercio().hashCode();
        }
        if (getDatosAdicionales() != null) {
            _hashCode += getDatosAdicionales().hashCode();
        }
        if (getComision() != null) {
            _hashCode += getComision().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ANTADWSResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "ANTADWSResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("respCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "RespCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numAuth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "NumAuth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensajeTicket");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "MensajeTicket"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mensajeCajero");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "MensajeCajero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("folioTransaccion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "FolioTransaccion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("folioComercio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "FolioComercio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datosAdicionales");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DatosAdicionales"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comision");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Comision"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
