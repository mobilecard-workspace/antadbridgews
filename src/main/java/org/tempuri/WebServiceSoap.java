/**
 * WebServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface WebServiceSoap extends java.rmi.Remote {
    public org.tempuri.ANTADWSResponse autorizacion(int comercio, java.lang.String sucursal, java.lang.String caja, java.lang.String cajero, java.lang.String horario, java.lang.String ticket, long folioComercio, java.lang.String operacion, java.lang.String referencia, java.math.BigDecimal monto, java.lang.String emisor, java.lang.String modoIngreso, java.math.BigDecimal comision, java.lang.String referencia2, java.lang.String referencia3, int reintento, java.lang.String FAC) throws java.rmi.RemoteException;
    public org.tempuri.ANTADWSResponse consulta(int comercio, java.lang.String sucursal, java.lang.String caja, java.lang.String cajero, java.lang.String horario, java.lang.String ticket, long folioComercio, java.lang.String operacion, java.lang.String referencia, java.math.BigDecimal monto, java.lang.String emisor, java.lang.String modoIngreso, java.math.BigDecimal comision, java.lang.String referencia2, java.lang.String referencia3, int reintento, java.lang.String FAC) throws java.rmi.RemoteException;
    public org.tempuri.RegistrosResponse registro(java.lang.String comercio, java.lang.String sucursal, java.lang.String macAddress) throws java.rmi.RemoteException;
    public org.tempuri.ConsultarEstatusResponse consultaEstatus(int comercio, java.lang.String sucursal, java.lang.String caja, java.lang.String cajero, java.lang.String horario, java.lang.String ticket, long folioComercio, java.lang.String operacion, java.lang.String referencia, java.math.BigDecimal monto, java.lang.String emisor, java.lang.String modoIngreso, java.math.BigDecimal comision, java.lang.String SKU, java.lang.String referencia2, java.lang.String referencia3) throws java.rmi.RemoteException;
    public org.tempuri.ANTADWSResponse reverso(int comercio, java.lang.String sucursal, java.lang.String caja, java.lang.String cajero, java.lang.String horario, java.lang.String ticket, long folioComercio, java.lang.String operacion, java.lang.String referencia, java.math.BigDecimal monto, java.lang.String emisor, java.lang.String modoIngreso, java.math.BigDecimal comision, java.lang.String referencia2, java.lang.String referencia3, int reintento, java.lang.String fechaContab) throws java.rmi.RemoteException;
}
